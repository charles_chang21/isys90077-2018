import { Template } from 'meteor/templating';
import { Residents } from '/imports/db';

import './main.html';

Session.setDefault('page', 'residentListTemplate');

Template.residentListTemplate.helpers({
  residents: function(){
    return Residents.find({});
  }
});

Template.body.helpers({
  currentPage: function(page){
     return Session.get('page');
  }
});


Template.residentListTemplate.events({
  'click #showAdmission': function(e, instance){
    e.preventDefault();
    Session.set('page', "admissionTemplate");
  }
});

Template.admissionTemplate.events({
  'click #showResidents': function(e, instance){
    e.preventDefault();
    Session.set('page', "residentListTemplate");
  },
  'click #save': function(e, instance){
    e.preventDefault();

    const room = $('#room').val();
    const firstName = $('#first_name').val();
    const lastName = $('#last_name').val();

    Residents.insert({
      Room: room,
      FirstName: firstName,
      LastName: lastName,
      createdAt: new Date(),
    });

    Session.set('page', "residentListTemplate");

  },
  'click #cancel': function(e, instance){
    e.preventDefault();
    Session.set('page', "residentListTemplate");
  }
});
